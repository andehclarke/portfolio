(function(thing){
    thing.fn.visible = function(partial) {
        var thingt = jQuery(this),
            thingw = jQuery(window),
            viewTop = thingw.scrollTop(),
            viewBottom = viewTop + thingw.height(),
            thetop = thingt.offset().top,
            thebottom = thetop + thingt.height(),
            compareTop = partial === true ? thebottom : thetop,
            compareBottom = partial === true ? thetop : thebottom;
            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));            
    };
})(jQuery);

function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

if(jQuery(window).width() >= 1024 && !isMobile()){    
    
    jQuery(window).scroll(function(event){
        jQuery(".slide").each(function(i, el){
            var el = jQuery(el);
            if (el.visible(true)) {
                el.addClass("come-in");
            }
        });
    });

    var win = jQuery(window);
    var allMods = jQuery(".slide")

    allMods.each(function(i, el){
        var el = jQuery(el);
        if (el.visible(true)) {
            el.addClass("already-visible");
        }
    });

    win.scroll(function(event){
        allMods.each(function(i, el){
            var el = jQuery(el);
            if (el.visible(true)) {
                el.addClass("come-in");
            }
        });
    });
};