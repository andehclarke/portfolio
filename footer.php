<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo">
					
					<div class="inner-footer grid-x grid-margin-x grid-padding-x">
						
						<div class="small-12 medium-12 large-12 cell">
							<nav role="navigation">
	    						<?php joints_footer_links(); ?>
	    					</nav>
						</div>
						
						<div class="small-12 medium-8 medium-offset-2 category" id="social">
							<div class="row center-all">
								<h3>Connect with me</h3>
							</div>
							<div class="row grid-x">
								<div class="small-12 center-all">
									<span>
										<a href="https://www.facebook.com/AndyClarkeWebServices/" target="_BLANK"><span class="socicon-facebook"></span></a> &middot;
										<a href="https://www.linkedin.com/in/andy-clarke-ba02b345/" target="_BLANK"><span class="socicon-linkedin"></span></a> &middot;
										<a href="https://codepen.io/andehclarke/"  target="_BLANK"><span class="socicon-codepen"></span></a>	
									</span>
								</div>
								<!-- <div class="auto cell center-all">
									<span class="socicon-github"></span>
								</div> -->
							</div>
						</div>
						<div class="cell small-12 center-all" id="copyright">
							<p class="source-org copyright">&copy; 2017 -  <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
							<p>Directory of <a href='http://www.nuneaton.co.uk/web-development/'>Nuneaton Web Development</a></p>
						</div>
					
					</div> <!-- end #inner-footer -->
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->