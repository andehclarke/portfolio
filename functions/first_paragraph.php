<?php

function firstparagraph() {
    // $first_paragraph_str = get_the_content();
    $first_paragraph_str = wpautop(get_the_excerpt());
    $first_paragraph_str = substr($first_paragraph_str, 0, strpos($first_paragraph_str, '</p>') + 4);
    $first_paragraph_str = strip_tags($first_paragraph_str, '<a><strong><em>');
    echo '<p class="first show-for-large">'.$first_paragraph_str.'</p>';
    $first_line = wpautop(get_the_excerpt());
    $first_line = substr($first_line, 0, strpos($first_line, '. ') + 1);
    $first_line = strip_tags($first_line, '<a><strong><em>');
    echo '<p class="first show-for-medium hide-for-large">'.$first_line.'</p>';
    $small = wpautop(get_the_excerpt());
    $small = substr($small, 0, 30);
    $small = strip_tags($small, '<a><strong><em>');
    echo '<p class="first show-for-small-only">'.$small.'[...]</p>';
}