<?php
/**
 * The homepage template file
 */

contact_email();

get_header(); ?>
			
	<div class="content">
	
		<div class="inner-content grid-x">
	
		    <main class="main small-12 medium-12 large-12 cell grid-x" role="main">
		    
                <div class="small-12 grid-x" id="main-header">
                    <div data-smooth-scroll class="small-10 small-offset-1 center-all">
                        <h1>
                            Hey, I'm Andy.
                            <br />
                            I design and develop websites for small businesses.
                        </h1>
                        <h3>I'm currently looking for new projects to work on.</h3>
                        <a href="#contact"><button type="button" class="button large">Get in touch</button></a>
                        <h2 class="bottom-readmore" data-smooth-scroll>
                            <a href="#menu-bottom"><span class="ss-pika ss-navigatedown"></span></a>
                        </h2>
                    </div>
                </div>
                <?php get_template_part( 'parts/nav', 'topbar' ); ?>
                <div class="small-12" data-sticky-container data-smooth-scroll id="menu-bottom">
                    <a href="#main-header">
                    <button class="button large sticky to-top" data-sticky data-stick-to="bottom" data-top-anchor="top-menu:bottom" data-sticky-on="small">
                        <span class="ss-pika ss-navigateup"></span>
                    </button>
                    </a>
                </div>
                <div class="small-12 medium-10 medium-offset-1 cat center-all" id="services">
                    <h2 class="slide">The Complete Package</h2>
                    <div class="grid-x grid-padding-x">
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="far fa-edit"></i>
                            </h2>
                            <h4>Design</h4>
                            <p>Starting with a blank page, I design beautiful experiences for your customers incorporating your branding.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="fas fa-wrench"></i>
                            </h2>
                            <h4>Development</h4>
                            <p>I'll make your website mobile friendly (responsive) and fast.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="fas fa-server"></i>
                            </h2>
                            <h4>Hosting</h4>
                            <p>Working with a prefered host I can help manage your hosting and liase between the techie stuff and what's important to you.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="far fa-envelope"></i>
                            </h2>
                            <h4>Emails</h4>
                            <p>All of my websites come with email addresses just waiting to be set up so that you can keep a consistent brand image.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="far fa-newspaper"></i>
                            </h2>
                            <h4>Content Management</h4>
                            <p>All of my websites come built on a content management system so you can keep on top of it or I can take care of that for you.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="fas fa-share-alt"></i>
                            </h2>
                            <h4>Social</h4>
                            <p>Keeping your brand active and recognisable on Social platforms helps drive traffic to your website.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="fas fa-shopping-cart"></i>
                            </h2>
                            <h4>Ecommerce</h4>
                            <p>Looking to sell online? Ecommerce is part of what I do.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="far fa-dot-circle"></i>
                            </h2>
                            <h4>Search Engine Optimisation</h4>
                            <p>I strive to make all of my designs search engine friendly to make sure you get the most out of your website.</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="far fa-life-ring"></i>
                            </h2>
                            <h4>Updates and Backups</h4>
                            <p>Security updates, keeping your website secure and taking regular backups. Boring, but essential. Why don't you let me take care of that?</p>
                        </div>
                        <div class="cell small-6 slide">
                            <h2>
                                <i class="fas fa-puzzle-piece"></i>
                            </h2>
                            <h4>Anything Else</h4>
                            <p data-smooth-scroll>If you're looking for anything else then feel free to <a href="#contact">send me a message</a>. I may be able to help, or at least point you in the right direction!</p>
                        </div>
                    </div>
                </div>
                <div class="small-10 small-offset-1 medium-8 medium-offset-2 cat center-all" id="work">
                    <h2 class="slide">My Work</h2>
                        
                    <?php query_posts('category_name=portfolio'); ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'parts/loop', 'portfolio' ); ?>
                    <?php endwhile; endif; ?>
                    <?php wp_reset_query(); ?>
                    
                </div>
                <div class="small-12 cat" id="contact">
                    <h2>Drop me a line</h2>
                    <form data-abide novalidate action="" method="POST">
                        <div data-abide-error class="alert callout" style="display: none;">
                            <p><i class="fi-alert"></i> There are some errors in your form.</p>
                        </div>
                        <div class="grid-x row">
                            <div class="small-12 large-6 columns">
                            <label>
                                <h2>Name</h2>
                                <input type="text" name="cf_name" placeholder="Your Name" required pattern="text" />
                                <span class="form-error">
                                Let me know who you are...
                                </span>
                            </label>
                            </div>
                            <div class="small-12 large-6 columns">
                            <label>
                                <h2>Email</h2>
                                <input type="email" name="cf_email" placeholder="example@email.com" required pattern="email" />
                                <span class="form-error">
                                Who do I reply to?
                                </span>
                            </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                            <label>
                                <h2>Message</h2>
                                <textarea rows="7" name="cf_message" placeholder="What you've got to share"></textarea>
                            </label>
                            </div>
                        </div>
                        <div class="row submit">
                            <input class="hide-me" name="anti-spam" type="text" />
                            <button class="button large" type="submit" value="Send">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="small-10 small-offset-1 medium-8 medium-offset-2 cat center-all" id="blog">
                    <h2 class="slide">Blog</h2>
                        
                    <?php query_posts('category_name=blog'); ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'parts/loop', 'andy_blog' ); ?>
                    <?php endwhile; endif; ?>
                    <?php wp_reset_query(); ?>
                    
                </div>
																								
		    </main> <!-- end #main -->
		    
		    <?php // get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>