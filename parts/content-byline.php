<?php
/**
 * The template part for displaying an author byline
 */
?>

<p class="byline">
	Added on <?php the_time('F j, Y') ?>
</p>	