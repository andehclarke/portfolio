<?php
/**
 * Template part for displaying my blog on the homepage
 *
 */
?>		
<div class="grid-x row blog-line slide">
    <div class="columns small-12 center-all">
        <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
        <?php firstparagraph(); ?>
    </div>
</div>