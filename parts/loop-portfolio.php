<?php
/**
 * Template part for displaying portfolio items on the homepage
 *
 */
?>		
<a href="<?php the_permalink() ?>" class="grid-x row portfolio-line slide">                                           
    <div class="columns small-12 medium-4">
        <?php the_post_thumbnail('full'); ?>
    </div>
    <div class="columns small-12 medium-8 text-left">
        <h3><?php the_title(); ?></h3>
        <?php firstparagraph(); ?>
    </div>
</a>