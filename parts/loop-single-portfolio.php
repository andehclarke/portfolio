<?php
/**
 * Template part for displaying a single post in portfolio category
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('grid-x grid-padding-x'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header grid-x grid-padding-x small-12" id="portfolio-header">
		<div class="small-12 medium-8">
			<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
        </div>
		<div class="small-12 medium-4 cell">
			<?php //the_post_thumbnail('medium_large'); ?>
		</div>
    </header> <!-- end article header -->
					
    <section id="portfolio-content" class="entry-content grid-x grid-padding-x small-12" itemprop="articleBody">
		<div class="small-12 medium-8">
			<p class="tags"><?php the_tags('<button type="button" class="button tech-tags">', '</button><button type="button" class="button tech-tags">', '</button>'); ?></p>
			<p><?php the_content() ?></p>
		</div>
		<!-- <div class="small-12 medium-4 cell" data-sticky-container style="height: 194px;">
			<div class="sticky" data-sticky data-stick-to="top" data-top-anchor="portfolio-header:top" data-btm-anchor="portfolio-content:bottom">
				<a href="<?php //echo get_post_meta($post->ID, 'url', true); ?>">
					<p><?php //the_post_thumbnail('medium_large'); ?></p>
					<p><?php //echo get_post_meta($post->ID, 'url', true); ?></p>
				</a>
			</div>
		</div> -->
		<div class="small-12 medium-4 cell">
			<a href="<?php echo get_post_meta($post->ID, 'url', true); ?>">
				<p><?php the_post_thumbnail('medium_large'); ?></p>
				<p><?php echo get_post_meta($post->ID, 'url', true); ?></p>
			</a>
		</div>
		<div class="small-12 medium-8">
			<?php //the_content(); ?>
		</div>
		
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
	</footer> <!-- end article footer -->
						
	<?php comments_template(); ?>	
													
</article> <!-- end article -->