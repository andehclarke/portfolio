<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header grid-x grid-padding-x">
		<div class="small-12 medium-4">
			<?php the_post_thumbnail('medium_large'); ?>
		</div>
		<div class="small-12 medium-8 cell">
			<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
			<p><a href="<?php echo get_post_meta($post->ID, 'url', true); ?>" target="_BLANK"><?php echo get_post_meta($post->ID, 'url', true); ?></a></p>
		</div>
		
    </header> <!-- end article header -->
					
    <section class="entry-content grid-x grid-padding-x" itemprop="articleBody">
		<div class="small-12">
		<p class="tags"><?php the_tags('<button type="button" class="button tech-tags">', '</button><button type="button" class="button tech-tags">', '</button>'); ?></p>
		</div>
		<div class="small-12">
			<?php the_content(); ?>
		</div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
	</footer> <!-- end article footer -->
						
	<?php comments_template(); ?>	
													
</article> <!-- end article -->