<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>
<div class="small-12" id="top-menu" data-sticky-container>
	<div class="top-bar sticky center-all" id="top-bar-menu" data-sticky data-stick-to="top" data-top-anchor="top-menu:top" data-sticky-on="small" data-options="marginTop:0;">
		<!-- <div class="top-bar-left float-left">
			<ul class="menu">
				<li><a href="<?php //echo home_url(); ?>"><?php //bloginfo('name'); ?></a></li>
			</ul>
		</div> -->
		<div class="show-for-medium">
			<?php joints_top_nav(); ?>	
		</div>
		<div class="show-for-small-only">
			<ul class="menu">
				<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
				<li><a data-toggle="off-canvas"><?php _e( '<i class="fas fa-bars"></i>', 'jointswp' ); ?></a></li>
			</ul>
		</div>
	</div>
</div>
