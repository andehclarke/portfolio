<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/responsive-navigation/
 */
?>

<div class="small-12" id="top-menu" data-sticky-container>
	<div class="top-bar sticky center-all" id="top-bar-menu" data-sticky data-stick-to="top" data-top-anchor="top-menu:top" data-sticky-on="small" data-options="marginTop:0;">
		<?php joints_top_nav(); ?>
	</div>
</div>