<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
			
<div class="content">

	<div class="inner-content grid-x grid-margin-x">

		<main class="main small-10 small-offset-1" role="main">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php if ( in_category('blog') ) {
					get_template_part( 'parts/loop', 'single-blog' );
				} elseif ( in_category('portfolio') ) {
					get_template_part( 'parts/loop', 'single-portfolio' );	
				} else {
					get_template_part( 'parts/loop', 'single' ); 
				}?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

		<?php //get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>